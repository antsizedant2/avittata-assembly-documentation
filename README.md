# Timeline:
[TOC]
## Rawreads:

### Concatenated all .fastq.gz files from fastq_pass from 2023SEP11 and 2023SEP25 into a single .fastq.gz file.

<details>
  <summary>Script</summary>

```
cat /archive/projects/PromethION/OrgOne/xferAvittata/unzip/2023SEP11_OrgOne_Avittata_V14/basecalling/pass/*.fastq.gz /archive/projects/PromethION/OrgOne/xferAvittata/unzip/2023SEP25_OrgOne_Avittata_Rpt/basecalling/pass/*.fastq.gz > 2023SEP11-25_OrgOne_Avittata.fastq.gz
```
</details>


### Converted .fastq.gz to .fasta.

<details>
  <summary>Script</summary>

```
module load seqtk

seqtk seq -a 2023SEP11-25_OrgOne_Avittata.fastq.gz > 2023SEP11-25_OrgOne_Avittata.fasta
```
</details>

### Ran centrifuge/1.0.4-beta on .fasta file.

<details>
  <summary>Script</summary>

```
module load centrifuge/1.0.4-beta

centrifuge -f \
        -x /core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v/abv \
        --report-file report.tsv \
        --quiet \
        --min-hitlen 50 \
        --threads 16 \
        -U /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/AvittataTest/01_raw_reads/2023SEP11-25_OrgOne_Avittata.fasta
```
</details>

### Ran kmerfreq on unfiltered .fasta file.

<details>
  <summary>Script</summary>

```
# calculate k-mer frequency using kmerfreq
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

echo "complete calculated k-mer frequency using kmerfreq"

less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum
```
</details>

Kmer individual number: `69722469739`

### Ran NanoPlot/1.33.0 on unfiltered .fastq.gz and .fasta file.

<details>
  <summary>Script</summary>

```
module load NanoPlot/1.33.0

NanoPlot --fastq 2023SEP11-25_OrgOne_Avittata.fastq.gz  --loglength --verbose -o summary-fastqpass -t 10 -p summary-fastqpass
```
</details>

Read lengths vs. Average read quality dot graph:

![nanoplotbefore](/Figures/unfiltered_nanoplot.png)

Unfiltered NanoPlot data:
| NanoPlot | Raw Reads |
|---|---|
| Mean read length  | 10,084.5  |
|  Mean read quality |    18.8|
|Median read length   | 6,286.0   |
|   Median read quality| 19.5  |
| Number of reads  |    6,924,796.0 |
|   Read length N50| 20,332.0  |
| STDEV read length  |   11,506.4 |
|Total bases |   69,833,266,465.0|

## RemoveContaminants:

### Ran GCE in homozygous and heterozygous mode on unfiltered .fasta file.

<details>
  <summary>Script</summary>

```
# run GCE in homozygous mode
 /core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 69722469739 -f read_files.lib.kmer.freq.stat.2colum >gce.table 2>gce.log

# run GCE in heterozygous mode 
 /core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 69722469739 -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log
```
</details>

Estimated unfiltered genome size:

| Homozygous | Heterozygous   |
|---|---|
|1.39463G  | 1.39463G |

Estimated coverage: `48.04x`

### Created coverage graph using RStudio.

<details>
  <summary>Script</summary>

```
library(readxl)

# Load the data from the Excel file (Please replace the path with the actual path to your Excel file)
Coverage_graph <- read_excel("/Users/anthonyhe/Downloads/read_files.lib.kmer.freq.stat.2colum.xlsx")

# View the loaded data
View(Coverage_graph)

# Set the default width and height for higher resolution plots
width_inches <- 8  # Adjust this value as needed
height_inches <- 6  # Adjust this value as needed

# Set the PDF options
pdf.options(width = width_inches, height = height_inches)

# Your plot code using Coverage_graph
plot(Coverage_graph[10:200,], type = "l", xlab = "Kmer Count", ylab = "Kmer Frequency", main = "Kmer Frequency vs. Count Graph For Avittata_filtered", col = "palegreen3", lwd = 5.0)
```
</details>

Graph:

![kmerfreqgraph](/Figures/kmerfreq_graph.png)

### Removed contaminants using grep and awk.

<details>
  <summary>Script</summary>

```
module load seqkit

# taking the classified reads from the STDOUT file and adding those to a new file 

grep -vw "unclassified" ../centrifuge_7396609.out > contaminated_reads.txt

# extracting just the IDs from the text file

awk NF=1 contaminated_reads.txt > contaminated_read_ids.txt

# keeping only unique IDs and removing any duplicates

sort -u contaminated_read_ids.txt > no_dup_centrifuge_contaminated_read_ids.txt

# adds only the sequences that DO NOT match the headers in the file we provided

seqkit grep -v -f no_dup_centrifuge_contaminated_read_ids.txt 2023SEP11-25_OrgOne_Avittata.fastq.gz > ../2023SEP11-25_OrgOne_Avittata_filtered.fastq
```
</details>

### Created pavian graph of contaminants.

<details>
  <summary>Script</summary>

```
module load centrifuge/1.0.4-beta
index=/core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v
centrifuge-kreport -x $index/abv ../report.tsv > pavian_report.txt
```
</details>

Graph:

![pavianreport](/Figures/pavian_graph.png)

### Ran kmerfreq on filtered .fasta file.

<details>
  <summary>Script</summary>

```
# calculate k-mer frequency using kmerfreq
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

echo "complete calculated k-mer frequency using kmerfreq"

# extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum
```
</details>

Kmer individual number: `69600552852`

### Ran NanoPlot/1.33.0 on filtered .fastq.gz and .fasta file.

<details>
  <summary>Script</summary>

```
module load NanoPlot/1.33.0

NanoPlot --fastq ../Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fastq --loglength --verbose -o summary-fastqpass -t 10 -p summary-fastqpass
```
</details>

Graph:

![nanoplotafter](/Figures/filtered_nanoplot.png)

Filtered NanoPlot data:
| NanoPlot | Raw Reads |
|---|---|
| Mean read length  | 10,078.8  |
|  Mean read quality |    18.8|
|Median read length   | 6,282.0   |
|   Median read quality| 19.5  |
| Number of reads  |    6,916,588.0 |
|   Read length N50| 20,324.0  |
| STDEV read length  |   11,496.7 |
|Total bases |   69,711,218,250.0|

## Assembly

### Ran GCE in homozygous and heterozygous mode on filtered .fasta file.

<details>
  <summary>Script</summary>

```
# run GCE in homozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 69600552852 -f read_files.lib.kmer.freq.stat.2colum >gce.table 2>gce.log

# run GCE in heterozygous mode 
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 69600552852 -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log
```
</details>

Estimated filtered genome size:

| Homozygous | Heterozygous   |
|---|---|
|1.39246G  | 1.39246G |

Estimated Coverage: `48.12x`

### Ran canu/2.2 on filtered .fastq.gz file, only taking reads above 5kb in length.

<details>
  <summary>Script</summary>

```
module load gnuplot/5.2.2
module load canu/2.2

echo "begin"

canu -p canu -d Canu_5kb \
        genomeSize=1400m \
        -minReadLength=5000 \
        -gridOptions="--partition=general --qos=general" canuIteration=1 \
        -nanopore /core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fasta
```
</details>

### Ran flye/2.9.1 on filtered .fastq.gz file.

<details>
  <summary>Script</summary>

```
module load flye/2.9.1

flye --nano-hq /core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fasta --no-alt-contigs --threads 32 --out-dir flye-output --scaffold
```
</details>

### Ran flye/2.9.1 on filtered .fastq.gz file with coverage specified.

<details>
  <summary>Script</summary>

```
flye --nano-hq /core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fasta --no-alt-contigs --threads 32 --out-dir flye-output --scaffold --asm-coverage 50 --genome-size 1400M
```
</details>

### Ran flye/2.9.1 on Canu-corrected .fastq.gz file with coverage specified.

<details>
  <summary>Script</summary>

```
flye --nano-hq /core/projects/EBP/conservation/prparrot/03_assembly/Canu_5kb/Canu_5kb/canu.correctedReads.fasta.gz --no-alt-contigs --threads 32 --out-dir flye-output --scaffold --asm-coverage 50 --genome-size 1400M
```
</details>

### Ran hifiasm on filtered.fastq.gz file.

<details>
  <summary>Script</summary>

```
hifiasm=/core/projects/EBP/conservation/software/hifiasm/hifiasm
reads=/core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fastq

$hifiasm -o avittata.asm -t 32 $reads
```
</details>

### Ran hifiasm on Canu-corrected .fastq.gz file.

<details>
  <summary>Script</summary>

```
hifiasm=/core/projects/EBP/conservation/software/hifiasm/hifiasm
reads=/core/projects/EBP/conservation/prparrot/03_assembly/Canu_5kb/Canu_5kb/canu.correctedReads.fasta.gz

$hifiasm -o avittata.asm -t 32 $reads
```
</details>

### Extracted hifiasm .fasta from bp.p_ctg.gfa file.

<details>
  <summary>Script</summary>

```
awk '/^S/{print ">"$2;print $3}' avittata.asm.bp.p_ctg.gfa > avittata.asm.bp.p_ctg.fasta
```
</details>

## QC

### Ran busco/5.4.5 + quast/5.2.0 on assembly output files.

<details>
  <summary>Script</summary>

```
fasta=/core/projects/EBP/conservation/prparrot/03_assembly/Hifiasm/avittata.asm.bp.p_ctg.fasta

module load busco/5.4.5

busco -i $fasta \
     -o busco_output \
     -l aves_odb10 \
     -m genome \
     -c 15

module load quast/5.2.0

quast.py $fasta \
       --threads 10 \
       -o quast_output
```
</details>

### Found best kmer size to use for building Meryl database.

<details>
  <summary>Script</summary>

```
module load singularity

singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif best_k.sh 1400000
```
</details>

### Built Meryl database using filtered .fasta file, removing kmers that appear only once.

<details>
  <summary>Script</summary>

```
# Load Singularity
module load singularity

# Define input and output file names
input_files=("/core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fastq.gz")
output_kmer_db="kmer_db.meryl"
output_filtered_kmer_db="kmer_db.filtered.meryl"

# Execute meryl count
singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif meryl count \
    threads=32 \
    k=21 \
    $input_files \
    output $output_kmer_db

# Execute meryl greater-than
singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif meryl greater-than 1 \
    threads=32 \
    k=21 \
    output $output_filtered_kmer_db $output_kmer_db
```
</details>

### Ran Merqury using filtered Meryl database.

<details>
  <summary>Script</summary>

```
# Load Singularity
module load singularity

# Define input and output file names
output_filtered_kmer_db="/core/projects/EBP/conservation/prparrot/04_initial_assembly_evaluation/kmer_db.filtered.meryl"
output_fasta="/core/projects/EBP/conservation/prparrot/03_assembly/Flye_cov/flye-output/assembly.fasta"
output_prefix="Avittata_flye_cov"

# Execute merqury.sh
singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif merqury.sh \
    $output_filtered_kmer_db \
    $output_fasta \
    $output_prefix
```
</details>

## ErrorCorrection + Purge

### Ran medaka/1.7.1 on chosen assemblies.

<details>
  <summary>Script</summary>

```
module load medaka/1.7.1
module unload tabix/0.2.6

reads=/core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fasta
assembly=/core/projects/EBP/conservation/prparrot/03_assembly/Flye_cov/flye-output/assembly.fasta

medaka_consensus -t 32 -i $reads -d $assembly -o medaka_output
```
</details>

### Mapped reads to assembly files using minimap2/2.15, converting .sam to .bam with samtools/1.9.

<details>
  <summary>Script</summary>

```
module load minimap2/2.15
module load samtools/1.9

reads=/core/projects/EBP/conservation/prparrot/02_quality_control/Centrifuge/2023SEP11-25_OrgOne_Avittata_filtered.fasta
assembly=/core/projects/EBP/conservation/prparrot/05_error_correction/medaka_flye_cov/medaka_output/consensus.fasta

minimap2 -t 16 -ax map-ont ${assembly} ${reads} \
       | samtools view -hF 256 - \
       | samtools sort -@ 16 -m 1G -o aligned.bam -T flye_tmp.ali
```
</details>

### Created kmer read-depth histogram using purge_haplotigs/1.1.2.

<details>
  <summary>Script</summary>

```
purge_haplotigs readhist -b aligned.bam -g ${assembly} -t 12 -d 150
```
</details>

### Ran purge_haplotigs/1.1.2 on chosen assemblies.

<details>
  <summary>Script</summary>

```
# use the cutoff values from histogram
purge_haplotigs contigcov -i aligned.bam.gencov -l 8 -m 32 -h 144
purge_haplotigs purge -b aligned.bam -g ${assembly} -c coverage_stats.csv -d
```
</details>

## Scaffolding

Note: From this point forward all non-relevant 'experimental' runs will no longer be mentioned as they were mostly discarded for good reason.

### Downloaded all existing draft _Amazona_ assemblies from https://www.ncbi.nlm.nih.gov/datasets/genome/?taxon=12929

<details>
  <summary>Script</summary>

```
datasets download genome accession $ACCESSION --include genome

```
</details>

<details>
  <summary>Accessions used</summary>

GCA_017639355.1
GCA_003947215.1
GCA_000332375.2
GCA_013399615.1
GCA_032357765.1
GCA_032360045.1
GCA_034782355.1
GCA_031763625.1
GCA_031763935.1
GCA_026283615.1
GCA_026283665.1
GCA_032357305.1
GCA_024331665.1
GCA_031763565.1
GCA_032353435.1
GCA_031763605.1
GCA_034782995.1
GCA_034782475.1
GCA_032468075.1
GCA_001420675.1
GCA_026283685.1
GCA_032360005.1
GCA_034782755.1
</details>

### Ran MeDuSa/1.6 using all draft _Amazona_ assemblies

<details>
  <summary>Script</summary>

```
medusa -f $refdir -i $genome -o scaffolded.fa -v
```
</details>

### Downloaded _Myiopsitta monachus_ assembly from accession GCA_017639245.1

At the recommendation of Taylor Hains for anchoring the assembly.

### Ran RagTag/2.1.0 on MeDuSa output using the _Myiopsitta monachus_ assembly as a reference.

<details>
  <summary>Script</summary>

```
ragtag.py scaffold $reference $assembly -f 1000
```
</details>

## Pore-C

### QC of the Pore-C data were performed identically to the initial Nanopore reads.

### Ran wf-pore-c v1.2.2-g9ce4a1b pipeline for processing and aligning the reads to the assembly. 

<details>
  <summary>Script</summary>

```
FASTQ=/core/projects/EBP/conservation/prparrot/13_porec/packages/wf-pore-c/2024JUN19_RON_PRParrot_PoreC_LSK114.fastq.gz
REF=/core/projects/EBP/conservation/prparrot/11_test_questionmarkquestionmarkquestionmark/MeDuSa_RagTag/mmon/ragtag_output/ragtag.scaffold.fasta

nextflow run epi2me-labs/wf-pore-c \
     -profile singularity \
     --fastq $FASTQ \
     --ref $REF \
     --cutter NlaIII \
     --paired_end_minimum_distance 100 --paired_end_maximum_distance 200 --pairs \
     --threads 24

# NlAIII was the enzyme used during preparation of the Pore-C data.
```
</details>

### Ran YaHS/1.2 on the final MeDuSa + RagTag assembly.

<details>
  <summary>Script</summary>

```
fastaFILLER=/core/projects/EBP/conservation/prparrot/11_test_questionmarkquestionmarkquestionmark/MeDuSa_RagTag/mmon/ragtag_output/ragtag.scaffold.fasta
pairsFILLER=/core/projects/EBP/conservation/prparrot/13_porec/03_scaffolding/alignment/output/pairs/2024JUN19_RON_PRParrot_PoreC_LSK114.pairs
yahsFILLER=/core/projects/EBP/conservation/prparrot/13_porec/packages/yahs

module load samtools/1.9
module load bedtools/2.29.0
module load cuda/9.0

samtools faidx $fastaFILLER
$yahsFILLER/yahs --file-type PA5 $fastaFILLER $pairsFILLER -e CATG
$yahsFILLER/juicer pre -a -o out_JBAT yahs.out.bin yahs.out_scaffolds_final.agp $fastaFILLER.fai > out_JBAT.log 2>&1 
set +o posix
mkdir tmp
(java -jar -Xms64G -Xmx150G -Djava.io.tmpdir=$PWD/tmp /core/globus/cgi/RAMP/assembly/porec/porec-nf2/wf-pore-c/yahs/yahs/juicer_tools.1.9.9_jcuda.0.8.jar pre out_JBAT.txt out_JBAT.hic.part <(cat out_JBAT.log  | grep PRE_C_SIZE | awk '{print $2" "$3}')) && (mv out_JBAT.hic.part out_JBAT.hic)
$yahsFILLER/juicer post -o first_JBAT out_JBAT.assembly out_JBAT.liftover.agp $fastaFILLER
```
</details>

### Manually curated scaffold edges according to juicebox results.

Juicebox:

![juiceboxpic](/Figures/juicebox.png)

## Analysis

### Ran fastp/0.23.2 on 300bp target insert size Illumina data from PuertoRicanParrot.fastq.xz.

<details>
  <summary>Script</summary>

```
reads=/core/projects/EBP/conservation/prparrot/05_error_correction/illumina_reads/PuertoRicanParrot.fastq

awk 'NR%2==1 { print $0 "/1" } ; NR%2==0 { print substr($0,length($0)/2+1) }' $reads > in.R1.fq
awk 'NR%2==1 { print $0 "/2" } ; NR%2==0 { print substr($0,length($0)/2+1) }' $reads > in.R2.fq

fastp \
   --in1 in.R1.fq \
   --in2 in.R2.fq \
   --out1 out.R1.trimmed.fastq.gz \
   --out2 out.R2.trimmed.fastq.gz \
   --json report_new_fastp.json \
   --html report_new_fastp.html
```
</details>

Fastp Results:

|                  | Before filtering | After filtering |
| ---------------- | ---------------- | --------------- |
| Total Reads      | 71531268         | 69462292        |
| Total Bases      | 11193481886      | 11140789211     |
| Q20 Bases        | 7679525889       | 7664884554      |
| Q30 Bases        | 130171346        | 130120764       |
| Mean Length      | 156              | 160             |
| GC Content       | 0.43843          | 0.43848         |

Filtering Results:

|                      |          |
| -------------------- | -------- |
| Passed Filter Reads: | 73105685 |
| Low Quality Reads:   | 1049297. |
| Too Many N Reads:    | 0        |
| Too Short Reads:     | 127791.  |
| Too Long Reads:      | 0        |


*Note: I never used this data.

### Ran RepeatModeler/2.0.5, RepeatMasker/4.1.7-p1, and TEtrimmer/1.4.0 on final curated assembly.

<details>
  <summary>Script</summary>

```
GENOME=/core/projects/EBP/conservation/prparrot/13_porec/07_curation/update/post_JBAT.FINAL.fa

module load singularity/biosim-3.10.0

singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif BuildDatabase -name "rm_database" $GENOME
singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif RepeatModeler -threads 30 -database rm_database -LTRStruct

TEtrimmer -i rm_database-families.fa -g $GENOME --dedup --min_blast_len 60 --classify_unknown
singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif RepeatMasker -dir tetrimmer_repeatmasker_out -pa 30 -lib TEtrimmer_output*/TEtrimmer_consensus.fasta -gff -a -noisy -xsmall $GENOME

mkdir eval
singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif buildSummary.pl tetrimmer_repeatmasker_out/*.out > eval/out.summary
singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif calcDivergenceFromAlign.pl -s out.div tetrimmer_repeatmasker_out/*.align
singularity exec /core/labs/Oneill/mneitzey/Software/dfam-tetools-1-89-2.sif createRepeatLandscape.pl -div out.div -g $(grep -v "^>" $GENOME | wc -c) > eval/landscape.html
```
</details>

Transposable element Kimura Divergence vs % of Genome graph:

![kimuraplot](/Figures/kimuraplot.png)

Transposable element family statistics:

```
Repeat Classes
==============
Total Sequences: 122
Total Length: 1182521291 bp
Class                  Count        bpMasked    %masked
=====                  =====        ========     =======
DNA                    --           --           --   
    TcMar-Pogo         333          106861       0.01% 
LINE                   --           --           --   
    CR1                241311       120723248    10.21% 
    RTE-BovB           131          163990       0.01% 
LTR                    196          112996       0.01% 
    ERV                705          545892       0.05% 
    ERV1               3675         2204003      0.19% 
    ERVK               3930         4540476      0.38% 
    ERVL               38286        22938117     1.94% 
    Ngaro              774          9142424      0.77% 
    Unknown            631          224766       0.02% 
SINE                   --           --           --   
    5S                 117          29709        0.00% 
Unknown                9848         2785720      0.24% 
                      ---------------------------------
    total interspersed 299937       163518202    13.83%

Low_complexity         46488        2887824      0.24% 
Satellite              755          792532       0.07% 
Simple_repeat          206900       12124434     1.03% 
---------------------------------------------------------
Total                  554080       179322992    15.16%
```

### Ran HiSat2/1.16.1 using newly generated RNA data.

Alignment rates were obtained using a nextflow pipeline with default parameters. Newly generated RNA data was processed identically to the previous Illumina data.
Final RNA alignment rate was `97.28%`.


# Pre-scaffolding + uncorrected assembly stats

| Reads                               |            |                  |              |                       |               |                        |                       |                                |                                      |                                              |            |                  |                 |                          |
|-------------------------------------|------------|------------------|--------------|-----------------------|---------------|------------------------|-----------------------|--------------------------------|--------------------------------------|----------------------------------------------|------------|------------------|-----------------|--------------------------|
| Assembly Stage                      | Canu_5kb   | Canu_5kb + Purge | Flye Default | Flye Default + Medaka | Flye Coverage | Flye Coverage + Medaka | Flye Coverage + Purge | Flye Coverage + Medaka + Purge | Flye Coverage + Canu-corrected reads | Flye Coverage + Canu-corrected reads + Purge | Hifiasm    | Hifiasm + Medaka | Hifiasm + Purge | Hifiasm + Medaka + Purge |
| BUSCO (Dataset: aves_odb10).        |            |                  |              |                       |               |                        |                       |                                |                                      |                                              |            |                  |                 |                          |
| Complete (Single & Duplicated)      | 97.0%      | 96.9%            | 97.0%        | 97.0%                 | 97.0%         | 97.0%                  | 97.0%                 | 97.0%                          | 96.9%                                | 96.9%                                        | 93.3%      | 93.6%            | 92.9%           | 93.2%                    |
| Single                              | 94.3%      | 96.4%            | 96.5%        | 96.5%                 | 96.5%         | 96.5%                  | 96.6%                 | 96.6%                          | 96.4%                                | 96.4%                                        | 80.2%      | 80.5%            | 87.1%           | 87.4%                    |
| Duplicated                          | 2.7%       | 0.5%             | 0.5%         | 0.5%                  | 0.5%          | 0.5%                   | 0.4%                  | 0.4%                           | 0.5%                                 | 0.5%                                         | 13.1%      | 13.1%            | 5.8%            | 5.8%                     |
| Fragmented                          | 0.6%       | 0.6%             | 0.6%         | 0.7%                  | 0.6%          | 0.6%                   | 0.6%                  | 0.6%                           | 0.6%                                 | 0.6%                                         | 0.8%       | 0.7%             | 0.9%            | 0.7%                     |
| Missing                             | 2.4%       | 2.5%             | 2.4%         | 2.3%                  | 2.4%          | 2.4%                   | 2.4%                  | 2.4%                           | 2.5%                                 | 2.5%                                         | 5.9%       | 5.7%             | 6.2%            | 6.1%                     |
| QUAST                               |            |                  |              |                       |               |                        |                       |                                |                                      |                                              |            |                  |                 |                          |
| Number of contigs                   | 2324       | 611              | 1439         | 1439                  | 1529          | 1529                   | 752                   | 781                            | 869                                  | 554                                          | 2236       | 2236             | 1112            | 1077                     |
| Largest contig (bp)                 | 36231386   | 36231386         | 80831616     | 80953976              | 80836081      | 80949388               | 80836081              | 80949388                       | 38762437                             | 38762437                                     | 15123899   | 15164502         | 15123899        | 15164502                 |
| Total length (bp)                   | 1329242839 | 1224099155       | 1234535300   | 1236653285            | 1234393498    | 1236462666             | 1210567891            | 1212977401                     | 1195232475                           | 1182485969                                   | 1448558342 | 1450390236       | 1315414438      | 1313334061               |
| GC content (%)                      | 43.23      | 42.90            | 42.99        | 43.00                 | 42.99         | 43.00                  | 42.83                 | 42.84                          | 42.78                                | 42.69                                        | 42.97      | 42.98            | 42.95           | 42.94                    |
| N50 (bp)                            | 8659288    | 10441596         | 21425748     | 21466688              | 20542504      | 20580522               | 20542504              | 20580522                       | 9875778                              | 9875778                                      | 2676480    | 2682896          | 3004470         | 3063254                  |
| L50                                 | 40         | 34               | 17           | 17                    | 16            | 16                     | 16                    | 16                             | 39                                   | 39                                           | 155        | 155              | 132             | 131                      |
| #Ns per 100kb                       | 0.00       | 0.00             | 0.69         | 0.01                  | 0.58          | 0.00                   | 0.53                  | 0.00                           | 0.92                                 | 0.92                                         | 0.00       | 0.00             | 0.00            | 0.00                     |
| MERQURY                             |            |                  |              |                       |               |                        |                       |                                |                                      |                                              |            |                  |                 |                          |
| QV score (aligned to ONT reads)     | 44.5319    | 45.7691          | 47.7403      | 39.7046               | 47.7163       | 39.7005                | 48.2386               | 40.0525                        | 44.731                               | 45.1391                                      | 37.0258    | 37.8276          | 37.2194         | 38.8412                  |
| QV score (aligned to illumina data) | 23.0578    | 23.1678          | 23.1013      | 23.0166               | 23.0973       | 23.0132                | 23.1461               | 23.0673                        | 23.0075                              | 23.0749                                      | 23.1637    | 23.236           | 23.1949         | 23.2898                  |

# After scaffolding (Amazon aestiva)

Before the Pore-C data was available, I attempted scaffolding against just existing assemblies and these were the most promising. None of these were used though.

| Reads                           |                           |                                         |                                                        |
|---------------------------------|---------------------------|-----------------------------------------|--------------------------------------------------------|
| Assembly Stage                  | Canu_5kb + Purge + Ragtag | Flye Coverage + Medaka + Purge + Ragtag | Flye Coverage + Canu-corrected reads + Purge + Ragtag  |
| BUSCO (Dataset: aves_odb10).    |                           |                                         |                                                        |
| Complete (Single & Duplicated)  | 97.0%                     | 97.0%                                   | 97.0%                                                  |
| Single                          | 96.5%                     | 96.6%                                   | 96.6%                                                  |
| Duplicated                      | 0.5%                      | 0.4%                                    | 0.4%                                                   |
| Fragmented                      | 0.6%                      | 0.6%                                    | 0.5%                                                   |
| Missing                         | 2.4%                      | 2.4%                                    | 2.5%                                                   |
| QUAST                           |                           |                                         |                                                        |
| Number of contigs               | 348                       | 620                                     | 286                                                    |
| Largest contig (bp)             | 141085751                 | 139937970                               | 138021187                                              |
| Total length (bp)               | 1224125455                | 1212993501                              | 1182512769                                             |
| GC content (%)                  | 42.90                     | 42.84                                   | 42.69                                                  |
| N50 (bp)                        | 91104594                  | 90492089                                | 97096621                                               |
| L50                             | 6                         | 6                                       | 5                                                      |
| #Ns per 100kb                   | 2.15                      | 1.33                                    | 3.19                                                   |
| MERQURY                         |                           |                                         |                                                        |
| QV score (aligned to ONT reads) |                           |                                         |                                                        |
| QV score (aligned to ONT reads) |                           |                                         |                                                        |

# After scaffolding (MeDuSa + Ragtag)

All assemblies here are sequential and based on the final Flye Coverage + Canu-corrected reads + Purge assembly, which was chosen due to being the most contiguous in previous scaffolding attempts.

| Reads                          |            |            |            |            |
|--------------------------------|------------|------------|------------|------------|
| Assembly Stage                 | MeDuSa     | Ragtag     | PoreC      | Curation   |
| BUSCO (Dataset: aves_odb10).   |            |            |            |            |
| Complete (Single & Duplicated) |      97.0% |      96.9% |      99.0% |      99.0% |
| Single copy                    |      96.5% |      96.5% |      98.6% |      98.6% |
| Multi copy                     |       0.5% |       0.4% |       0.4% |       0.4% |
| Fragmented                     |       0.6% |       0.5% |       0.2% |       0.2% |
| Missing                        |       2.4% |       2.6% |       0.8% |       0.8% |
| QUAST                          |            |            |            |            |
| # contigs                      |        211 |         85 |        123 |         47 |
| Largest contig                 |   68574086 |  124090028 |  139904328 |  139904328 |
| Total length                   | 1182501069 | 1182513669 | 1182524169 | 1176382075 |
| GC (%)                         |      42.69 |      42.69 |      42.69 |      42.67 |
| N50                            |   24925486 |   84101064 |   92159505 |   92159505 |
| N90                            |    6451985 |   43069480 |   21629516 |   21629516 |
| auN                            |   28700598 | 83340552.4 | 87557244.6 | 88013082.2 |
| L50                            |         15 |          6 |          5 |          5 |
| L90                            |         53 |         13 |         15 |         15 |
| #Ns per 100kb                  |        2.2 |       3.26 |       4.15 |       3.93 |
